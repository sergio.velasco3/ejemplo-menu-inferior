package com.estech.bottomnavsample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.estech.bottomnavsample.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {


    private lateinit var binding: FragmentHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnNav.setOnClickListener {
            val texto = binding.etName.text.toString()
            if (texto.isEmpty())
                Toast.makeText(requireContext(), "No has escrito nada", Toast.LENGTH_SHORT).show()
            else {

                // creamos un bundle y lo pasamos como dato al siguiente fragment
                val bundle = bundleOf("name" to texto)
                findNavController().navigate(R.id.action_nav_home_to_otroFragment, bundle)

            }

        }
    }
}