package com.estech.bottomnavsample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.estech.bottomnavsample.databinding.FragmentOtroBinding

class OtroFragment : Fragment() {


    private lateinit var binding: FragmentOtroBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOtroBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // recuperación de datos de un bundle
        arguments?.let {
            val texto = it.getString("name")
            binding.tvNotif.text = texto
        }
    }
}