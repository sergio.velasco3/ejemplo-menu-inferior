package com.estech.bottomnavsample

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.estech.bottomnavsample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // el bottomnavigationview simplemente metido en una variable
        val navView = binding.bottomNavigationView

        // controlador de la navegación
        navController = findNavController(R.id.fragmentContainerView)
        // administra la navegación en la barra superior
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_notif, R.id.nav_settings
            )
        )
        // sincroniza barras superior con navegación
        setupActionBarWithNavController(navController, appBarConfiguration)
        // sincroniza el menú inferior con la navegación
        navView.setupWithNavController(navController)

        // este listener se activa cuando se navega
        // en este caso se va a usar para saber si estamos en uno de los 3 fragments iniciales o no
        // si estamos en uno de esos 3, mostramos el menú inferior
        // si navegamos a cualquier otro, se oculta
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.id == R.id.otroFragment)
                binding.bottomNavigationView.visibility = View.GONE
            else
                binding.bottomNavigationView.visibility = View.VISIBLE
        }
    }

    // función para que funciones el botón atrás de la barra superior
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}